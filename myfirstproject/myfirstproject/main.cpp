#include <iostream>
#include <string>

using namespace std;

int main()
{
    double a = 0;
    double b = 0;
    double c = 0;

    cout << "a = ";
    cin >> a;

    cin.ignore();

    cout << "b = ";
    cin >> b;

    cin.ignore();

    c = a + b;

    cout << "a + b = " << a << " + " << b << " = " << c << endl;

    return 0;
}
