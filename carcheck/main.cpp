#include "func.h"

using namespace std;

int main()
{

    vector<Vehicle *> vehicleList;

    bool stopWhile = false;
    int menu = -1;

    cout << "Hello world!" << endl;

    readDataFromCSV(vehicleList);

    while(!stopWhile)
    {
        printMenu();
        cin >> menu;
        cin.ignore();
        cout << endl;

        switch(menu)
        {
            case 0:
                stopWhile = true;
                cout << "Bye world!" << endl;
                break;

            case 1:
                printVehicleList(vehicleList);
                break;

            case 2:
                addVehicleToList(vehicleList);
                break;

            case 3:
                deleteVehicleFromList(vehicleList);
                break;

            case 4:
                printVehicleInfo(vehicleList);
                break;

            default:
                cout << "Unknown menu - Please try again" << endl;
                break;
        }
    }

    writeDataToCSV(vehicleList);

    return 0;
}
