#ifndef H_VEHICLE_DEF_H
#define H_VEHICLE_DEF_H

#include <iostream>
#include <string>

using namespace std;

enum Vehicle_Type {car, truck, moto, unknown_vt};
enum GearBox {manual, automatic, unknown_gb};

class Vehicle
{
    public:
        Vehicle();
        Vehicle(int km, int nbr_door, string color, string brand, string model, Vehicle_Type vtype, GearBox gearbox);
        virtual ~Vehicle();

        void printInfo() const;

        //Getters
        int getKm() const;
        int getNbrDoor() const;
        string getColor() const;
        string getBrand() const;
        string getModel() const;
        GearBox getGearBox() const;
        Vehicle_Type getVehicleType() const;

        //Setters
        void setKm(int km);
        void setNbrDoor(int nbr_door);
        void setColor(string color);
        void setBrand(string brand);
        void setModel(string model);
        void setGearBox(GearBox gearbox);
        void setVehicleType(Vehicle_Type vt);

    private:
        int m_kmeters;
        int m_nbr_door;
        string m_color;
        string m_brand;
        string m_model;

        Vehicle_Type m_vtype;
        GearBox m_gearbox;


        // To add later :
        // date of 1st circulation
        // date bought
};

#endif // H_VEHICLE_DEF_H
