#ifndef H_FUNCTION_DEF_H
#define H_FUNCTION_DEF_H

#include <iostream>
#include <bits/stdc++.h>
#include <string>
#include <vector>
#include <fstream>
#include "vehicle.h"

void printMenu();
void printVehicleList(vector<Vehicle *> const& vlist);
void addVehicleToList(vector<Vehicle *>& vlist);
void deleteVehicleFromList(vector<Vehicle *>& vlist);
void printVehicleInfo(vector<Vehicle *> const& vlist);

bool writeDataToCSV(vector<Vehicle *> const& vlist);
bool readDataFromCSV(vector<Vehicle *>& vlist);

#endif // H_FUNCTION_DEF_H
