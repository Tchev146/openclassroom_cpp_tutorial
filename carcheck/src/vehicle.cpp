#include "vehicle.h"

Vehicle::Vehicle()
{
    m_kmeters = 0;
    m_nbr_door = 0;
    m_color = "";
    m_brand = "";
    m_model = "";
    m_vtype = unknown_vt;
    m_gearbox = unknown_gb;
}

Vehicle::Vehicle(int km, int nbr_door, string color, string brand, string model, Vehicle_Type vtype, GearBox gearbox)
{
    m_kmeters = km;
    m_nbr_door = nbr_door;
    m_color = color;
    m_brand = brand;
    m_model = model;
    m_vtype = vtype;
    m_gearbox = gearbox;
}

Vehicle::~Vehicle()
{

}

//Getters
void Vehicle::printInfo() const
{
    cout << "##### Vehicul #####" << endl;
    cout << "Kilometers : " << m_kmeters << endl;
    cout << "Door Number : " << m_nbr_door << endl;
    cout << "Color : " << m_color << endl;
    cout << "Brand : " << m_brand << endl;
    cout << "Model : " << m_model << endl;

    cout << "Type : ";

    switch(m_vtype)
    {
        case car :
            cout << "Car";
            break;

        case truck:
            cout << "Truck";
            break;

        case moto:
            cout << "Moto";
            break;

        case unknown_vt:
            cout << "Unknown";
            break;
    }

    cout << endl;

    cout << "Gearbox : ";

    switch(m_gearbox)
    {
        case manual:
            cout << "Manual";
            break;

        case automatic:
            cout << "Automatic";
            break;

        case unknown_gb:
            cout << "Unknown";
            break;
    }

    cout << endl;
    cout << endl;
}

int Vehicle::getKm() const
{
    return m_kmeters;
}

int Vehicle::getNbrDoor() const
{
    return m_nbr_door;
}

string Vehicle::getColor() const
{
    return m_color;
}

string Vehicle::getBrand() const
{
    return m_brand;
}

string Vehicle::getModel() const
{
    return m_model;
}

GearBox Vehicle::getGearBox() const
{
    return m_gearbox;
}

Vehicle_Type Vehicle::getVehicleType() const
{
    return m_vtype;
}

//Setters
void Vehicle::setKm(int km)
{
    m_kmeters = km;
}

void Vehicle::setNbrDoor(int nbr_door)
{
    m_nbr_door = nbr_door;
}

void Vehicle::setColor(string color)
{
    m_color = color;
}

void Vehicle::setBrand(string brand)
{
    m_brand = brand;
}

void Vehicle::setModel(string model)
{
    m_model = model;
}

void Vehicle::setGearBox(GearBox gearbox)
{
    m_gearbox = gearbox;
}

void Vehicle::setVehicleType(Vehicle_Type vt)
{
    m_vtype = vt;
}
