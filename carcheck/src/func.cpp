#include "func.h"

void printMenu()
{
    cout << "Menu :" << endl;
    cout << "1 - View vehicle list" << endl;
    cout << "2 - Add a vehicle to the list" << endl;
    cout << "3 - Delete a vehicle from the list" << endl;
    cout << "4 - Print all info from a vehicle" << endl;
    cout << "0 - QUIT" << endl;
    cout << endl;
    cout << "Enter a number (0 - 6) ? ";
}

void printVehicleList(vector<Vehicle *> const& vlist)
{
    cout << "Vehicle list : " << endl;

    if (vlist.size() == 0)
    {
        cout << "List empty - No vehicle found" << endl;
    }

    for(uint8_t i = 0; i<vlist.size(); ++i)
    {
        cout << unsigned(i) << " - " << vlist.at(i)->getBrand() << " " << vlist.at(i)->getModel() << " " << vlist.at(i)->getKm() << endl;
    }

    cout << endl;

}

void addVehicleToList(vector<Vehicle *>& vlist)
{
    Vehicle * v = new Vehicle();

    int kmeters;
    int nbr_door;
    string color;
    string brand;
    string model;

    string vtype;
    string gearbox;


    cout << "Add a new vehicle to the list :" << endl;

    cout << "Brand ? " << endl;
    getline(cin, brand);

    cout << "Model ? " << endl;
    getline(cin, model);

    cout << "Color ? " << endl;
    getline(cin, color);

    cout << "Killometers ?" << endl;
    cin >> kmeters;
    cin.ignore();

    cout << "Door number ?" << endl;
    cin >> nbr_door;
    cin.ignore();

    cout << "Vehicle Type : (Car - Truck - Moto) ? " << endl;
    getline(cin, vtype);

    transform(vtype.begin(), vtype.end(), vtype.begin(), ::tolower);

    if (vtype == "car")
    {
        v->setVehicleType(car);
    }
    else if (vtype == "truck")
    {
        v->setVehicleType(truck);
    }
    else if (vtype == "moto")
    {
        v->setVehicleType(moto);
    }
    else
    {
        v->setVehicleType(unknown_vt);
    }

    cout << "GearBox : (Manual - Automatic) ? " << endl;
    getline(cin, gearbox);

    transform(gearbox.begin(), gearbox.end(), gearbox.begin(), ::tolower);

    if (gearbox == "manual")
    {
        v->setGearBox(manual);
    }
    else if (gearbox == "automatic")
    {
        v->setGearBox(automatic);
    }
    else
    {
        v->setGearBox(unknown_gb);
    }

    v->setBrand(brand);
    v->setModel(model);
    v->setColor(color);
    v->setKm(kmeters);
    v->setNbrDoor(nbr_door);

    vlist.push_back(v);

    cout << endl;
}

void deleteVehicleFromList(vector<Vehicle *>& vlist)
{
    int carId = -1;
    printVehicleList(vlist);
    cout << "Car id to delete ? " << endl;
    cin >> carId;
    cin.ignore();

    vlist.erase(vlist.begin() + carId) 
}

void printVehicleInfo(vector<Vehicle *> const& vlist)
{
    int carId = -1;
    printVehicleList(vlist);
    cout << "Car id ? " << endl;
    cin >> carId;
    cin.ignore();

    vlist.at(carId)->printInfo();
}

bool writeDataToCSV(vector<Vehicle *> const& vlist)
{
    string const filepath = "./data.csv";

    ofstream myfile;
    myfile.open(filepath.c_str());

    if (myfile)
    {
        cout << "Saving list to File : " << endl;
        cout << "File path : " << filepath << endl;

        if (vlist.size() == 0)
        {
            cout << "List empty - No vehicle found" << endl;
            myfile.close();
            return false;
        }

        for(uint8_t i = 0; i<vlist.size(); ++i)
        {
            Vehicle * v = vlist.at(i);
            myfile << v->getBrand() << "," << v->getModel() << "," << v->getColor() << "," << v->getKm() << "," << v->getNbrDoor() << "," << v->getVehicleType() << "," << v->getGearBox() << endl;
        }

        myfile.close();

        cout << endl;
    }
    else
    {
        cout << "Error could not open file" << endl;
        return false;
    }

    return true;

}

bool readDataFromCSV(vector<Vehicle *>& vlist)
{
    string const filepath = "./data.csv";
    string line = "";
    string delimiter = ",";
    string token;
    string::size_type sz;

    ifstream myfile;
    size_t pos = 0;
    int col = 0;

    Vehicle * v = new Vehicle();

    myfile.open(filepath.c_str());

    if(myfile)
    {
        cout << "Read list from File : " << endl;
        cout << "File path : " << filepath << endl;

        while(getline(myfile, line))
        {
            col = 0;
            while ((pos = line.find(delimiter)) != std::string::npos)
            {
                token = line.substr(0, pos);

                switch(col)
                {
                    case 0:
                        v->setBrand(token);
                        break;

                    case 1:
                        v->setModel(token);
                        break;

                    case 2:
                        v->setColor(token);
                        break;

                    case 3:
                        v->setKm(stoi(token,&sz));
                        break;

                    case 4:
                        v->setNbrDoor(stoi(token,&sz));
                        break;

                    case 5:
                        v->setVehicleType(static_cast<Vehicle_Type> (stoi(token,&sz)));
                        break;

                }

                line.erase(0, pos + delimiter.length());
                col++;
            }

            v->setGearBox(static_cast<GearBox> (stoi(line,&sz)));

            vlist.push_back(v);
        }

        myfile.close();

        cout << endl;
    }
    else
    {
        cout << "Error could not open file" << endl;
        return false;
    }

    return true;
}
